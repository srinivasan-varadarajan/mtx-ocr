# mtx_ocr

Welcome to the mtx_ocr repository!

## Quickstart

1. Clone the repository: `git clone git@bitbucket.org:modernatx/mtx_ocr.git`
2. Install the conda environment: `conda env create -f environment.yml`
3. Install the custom source package: `pip install -e .`
4. If using VSCode (recommended), ensure you're using the correct Python
   interpreter (this project's conda environment) via the command pallette.


Choose your own adventure:

4. Check out the analysis notebooks in the `notebooks/` directory
5. Preview the docs: `mkdocs serve`
6. Run the tests: `pytest .`

## Who to contact:

svaradarajan@modernatx.com
