"""Example test for mtx_ocr."""
from mtx_ocr.example import hello_world


def test_hello_world():
    """Test for the hello world function.

    Place intent of test here.
    """
    assert hello_world() == "Hello, world!"
