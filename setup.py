"""Setup shim for mtx_ocr.

This file works in tandem with setup.cfg.
Please see the following link for more information.

    https://setuptools.pypa.io/en/latest/userguide/quickstart.html?highlight=editable#development-mode
"""
from setuptools import setup

setup()
