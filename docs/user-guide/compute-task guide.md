# User Interface

## Input User Interface

![UI in compute task](../img/compute_task_img.png "UI of compute task")

### Expected input format and parameters

The compute task takes the following inputs.

    1. Upload one or more pdf files or image files
    2. oem (Which tesseract engine to use)- oem takes values from [0-3] and the default value is 3
    3. psm (Which page segmentation method to use) - psm takes values from [0-13] and the default value is 3

### What is oem?

oem refers to the OCR Engine mode, the value of oem determines which ocr engine is being utilized

### What does different values of OEM signify?

    0    Legacy engine only.
    1    Neural nets LSTM engine only. (This mode works the best)
    2    Legacy + LSTM engines.
    3    Default, based on what is available

## What is psm?

PSM refers to the page segmentation mode. By default tesseract expects a page when it segments an input image. If we are OCR'ing a single page of a document then the default psm value of 3 works well, but if we are OCR'ing one line in a page then the default value of 3 might not give the expected results

### What does different values of psm signify?

    0    Orientation and script detection (OSD) only.
    1    Automatic page segmentation with OSD.
    2    Automatic page segmentation, but no OSD, or OCR. (not implemented)
    3    Fully automatic page segmentation, but no OSD. (Default)
    4    Assume a single column of text of variable sizes.
    5    Assume a single uniform block of vertically aligned text.
    6    Assume a single uniform block of text.
    7    Treat the image as a single text line.
    8    Treat the image as a single word.
    9    Treat the image as a single word in a circle.
    10    Treat the image as a single character.
    11    Sparse text. Find as much text as possible in no particular order.
    12    Sparse text with OSD.
    13    Raw line. Treat the image as a single text line,
        bypassing hacks that are Tesseract-specific.

For further information on different PSM modes please refer to the following **[tutorial](https://pyimagesearch.com/2021/11/15/tesseract-page-segmentation-modes-psms-explained-how-to-improve-your-ocr-accuracy/)



## Output of Compute task

    The Compute task creates a folder for every image/pdf that was passed for ocr with the name of the image file/pdf file and the folder contains a csv file of the ocr output and the raw text output.

    The csv file contains the following columns:

        1) level : Its value tells us whether the row in the output is
                    1. a page
                    2. a block
                    3. a paragraph
                    4. a line
                    5. a word
        2) page_num: page on which the word was found
        3) block_num: tells the detected layout block within the current page
        4) par_num: paragraph number within the layout block
        5) line_num: line number within the paragraph
        6) word_num: word number within the line
        7) left: left refers to the left most coordinate (x1) if a rectangle was drawn around the word
        8) top: top refers to the y distance from the top of the document (y1) if a rectangle was drawn around the word
        9) width : width is the width of the rectangle (x1+width gives the other side of the rectange)
        10) hieght: hieght is the hieght of the rectange (y1+ hieght gives the hieght of the lowest pixel of the word or y2)
        11) conf : how confident the word is the given word
        12) text : the extracted text
        13) page# : page number of the OCR'ed word

The output of the compute task can be downloaded as a zip file to your local computer

### How can i use the compute task from python

    Please refer to the **[example notebook](/home/svaradarajan/projects/mtx-ocr/notebooks/example.ipynb)
