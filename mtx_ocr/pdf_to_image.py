# %%

""" This script converts a pdf of multiple pages into images with each page being an image

    functions:
        -pdf2img

"""
from pdf2image import convert_from_path


def pdf2img(pdf_path: str):

    """This function takes the dir of all pdfs as input and converts every page of pdf to image for OCR

    :param pdf_path: The location where all pdf files are present
    :type pdf_path: str
    :return: Returns a list of images
    :rtype: List[Images]

    """
    a = []
    pages = convert_from_path(pdf_path, 300)
    # pdf_file_name = str(pdf_path.stem)[:-4]
    # save_dir_pdf = os.path.join(save_dir, pdf_file_name)
    # print(save_dir_pdf)
    # os.mkdir(save_dir_pdf)

    for page in pages:
        # print(save_dir_pdf+"%s-page%d.jpg" % (pdf_file_name, pages.index(page)), "JPEG")
        a.append(page)

    return a
