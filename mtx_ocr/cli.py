"""CLI for mtx_ocr."""
import json
import os
from typing import Optional, Tuple

import pandas as pd
import typer

from mtx_ocr.image_pre_process import output_file
from mtx_ocr.ocr import ocr_to_text

# QUESTION (RVW): Why does pylance hate certain packages (Import "typer" could not be resolved)?


app = typer.Typer()


@app.command()
def ocr(
    file_path: str = typer.Argument("/data", help="The name of the file"),
    oem: Optional[int] = typer.Argument(3, help="Different OCR engine models, takes value from 0-3"),
    psm: Optional[int] = typer.Argument(3, help="Different OCR page segmentation model, takes value 0-13"),
) -> Tuple[list[pd.DataFrame], list[list[str]]]:
    """This function takes the filename and the dir in which the ocr output should be saved as input and saves the ocr output in save_dir

    :param file_path: The folder where the input files are present
    :type file_path: str
    :param oem: Different OCR engine models, takes value from 0-3, defaults to 3
    :type oem: int,Optional
    :param psm: Different OCR page segmentation model, takes value 0-13, defaults to 3
    :type psm: int,Optional
    :return: The dataframe with text and bounding boxes and a list of only the extracted text
    :rtype: Tuple[list[pd.DataFrame], list[list[str]]]
    """
    data_frames = []
    txt_output = []
    file = []
    for files in os.listdir(file_path):
        df, txt = ocr_to_text(os.path.join(file_path, files), oem, psm)
        name = files.split(".")[0]
        os.mkdir(os.path.join(file_path, name))
        df.to_csv(os.path.join(file_path, name, name + ".csv"))
        output_file(os.path.join(file_path, name, name + ".txt"), txt)
        data_frames.append(df.to_json())
        txt_output.append(txt)
        file.append(files)
    with open(os.path.join(file_path, "results.json"), "w") as f:
        json.dump({"file_names": file, "dataframes": data_frames, "text": txt_output}, f)

    return data_frames, txt_output


if __name__ == "__main__":
    app()
