""" This script contains all the functions required to preprocess an image before passing it to OCR

    functions:
        -output_file
        -process_image_for_ocr
        -set_image_dpi
        -image_smoothening
        remove_noise_and_smooth
"""


import tempfile

import cv2
import numpy as np
import PIL
from PIL import Image


def output_file(filename: str, data: list[str]) -> None:
    """This fucntion is used to write the output of OCR into a text file

    :param filename: The path of text file where the file needs to be saved
    :type filename: str
    :param data: ocr output data
    :type data: list[str]
    """
    with open(filename, "w+") as f:
        for item in data:
            f.write("%s\n" % item)

        f.close()
    return


IMAGE_SIZE = 1800
BINARY_THREHOLD = 180


def process_image_for_ocr(img: PIL.Image) -> np.ndarray:
    """This function preprocesses the image for OCR by setting the image dpi to 300 and removing noise from the image

    :param img: The loaded image file in PIL.Image format, defaults to None
    :type img: PIL.Image
    :return: preprocessed image
    :rtype: np.ndarray
    """
    temp_filename = set_image_dpi(img=img)
    im_new = remove_noise_and_smooth(file_name=temp_filename)
    return im_new


def set_image_dpi(img: PIL.Image) -> str:
    """make the dpi of image to 300 to get a good quality OCR

    :param img: The loaded image file in PIL.Image format, defaults to None
    :type img: Image, optional
    :return: temp file name of the image
    :rtype: str
    """
    length_x, width_y = img.size
    factor = max(1, int(IMAGE_SIZE / length_x))
    size = factor * length_x, factor * width_y
    # size = (1800, 1800)
    im_resized = img.resize(size, Image.ANTIALIAS)
    temp_file = tempfile.NamedTemporaryFile(delete=False, suffix=".jpg")
    temp_filename = temp_file.name
    im_resized.save(temp_filename, dpi=(300, 300))
    return temp_filename


def image_smoothening(img: np.ndarray) -> np.ndarray:
    """Perform ultiple cv thresholding to smoothen the image

    :param img: image to be smoothened
    :type img: Image
    :return: Smoothened Image
    :rtype: np.ndarray
    """
    ret1, th1 = cv2.threshold(img, BINARY_THREHOLD, 255, cv2.THRESH_BINARY)
    ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    blur = cv2.GaussianBlur(th2, (1, 1), 0)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return th3


def remove_noise_and_smooth(file_name: str) -> np.ndarray:
    """Preprocess the image to remove noise and smoothen the image

    :param file_name: Path to image file
    :type file_name: str
    :return: denoised image
    :rtype: np.ndarray
    """

    img = cv2.imread(file_name, 0)
    filtered = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 41, 3)
    kernel = np.ones((1, 1), np.uint8)
    opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    img = image_smoothening(img)
    or_image = cv2.bitwise_or(img, closing)
    return or_image
