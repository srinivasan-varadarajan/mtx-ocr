"""Example module for mtx_ocr."""


def hello_world() -> str:
    """Hello command

    :returns: A string.
    """
    return "Hello, world!"
