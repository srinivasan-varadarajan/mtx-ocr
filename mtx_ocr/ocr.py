""" This script performs ocr on a text or a image file

    functions:
        -ocr_to_text
## load the image inside the ocr to text and make the argument for preprocess ocr to take only the umage to remove the duplication of code
### make the writting of df and txt as seperate functions and call them in cli.py

"""

import os

import pandas as pd
import pytesseract
from PIL import Image
from pytesseract import Output

from mtx_ocr.image_pre_process import process_image_for_ocr
from mtx_ocr.pdf_to_image import pdf2img


def ocr_to_text(file_name: str, oem: int = 3, psm: int = 3) -> tuple[pd.DataFrame, list[str]]:

    """
    This function takes a pdf or image file as input and outputs a csv and a txt file of the text

    :param file_name: The path to the input file
    :type file_name: str
    :param oem: Different OCR engine models, takes value from 0-3,defaults to 3
    :type oem:int
    :param psm: Different OCR page segmentation model, takes value 0-13, defaults to 3
    :return: The DataFrame with bounding boxes and text
    :rtype: tuple[pd.DataFrame,list(str)]
    """
    formats = (".png", ".jpg", ".jpeg", ".tiff", ".pdf")
    assert type(file_name) == str, "filenames should be of type str"
    assert file_name.endswith(formats), "file should be a pdf or an image file"
    image_formats = (".png", ".jpg", ".jpeg", ".tiff")
    config = "--oem %d --psm %d" % (oem, psm)
    if not file_name.startswith("./data"):
        file_name = os.path.join("./data", file_name)

    txt = []
    df = []
    if file_name.endswith(".pdf"):
        list_img = pdf2img(file_name)
    elif file_name.endswith(image_formats):
        list_img = []
        img = Image.open(file_name)
        list_img.append(img)
    for i in range(len(list_img)):
        processed_img = process_image_for_ocr(list_img[i])
        txt.append(pytesseract.image_to_string(processed_img, config=config))
        pre_df = pytesseract.image_to_data(processed_img, output_type=Output.DICT, config=config)
        pre_df = pd.DataFrame.from_dict(pre_df)
        pre_df["page#"] = i
        df.append(pre_df)
    final_df = pd.concat(df)

    return final_df, txt
